//
//  ViewController.swift
//  ScoreboardS
//
//  Created by Cory Dunn on 9/21/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var lbl1: UILabel!
    @IBOutlet var lbl2: UILabel!
    @IBOutlet var btn: UIButton!
    @IBOutlet var stepper1: UIStepper!
    @IBOutlet var stepper2: UIStepper!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func stepper1Changed(sender:UIStepper) {
        self.lbl1.text = String(format:"%.f", sender.value);
    }
    
    @IBAction func stepper2Changed(sender:UIStepper) {
        self.lbl2.text = String(format:"%.f", sender.value);
    }
    
    @IBAction func resetBtn(sender:UIButton) {
        lbl1.text = String("0");
        lbl2.text = String("0");
        stepper1.value = 0;
        stepper2.value = 0;
    }


}

